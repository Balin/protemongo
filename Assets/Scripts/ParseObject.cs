﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Networking;

using Newtonsoft.Json;

public class ParseObject
{

    public string ClassName { get; private set; }

    public ParseObject()
    {

    }

    public ParseObject(string className)
    {
        ClassName = className;
    }

    public string ObjectId
    {
        get
        {
            return (string)properties["objectId"];
        }
        set
        {
            properties["objectId"] = value;
        }
    }

    public ParseAcl Acl
    {
        get
        {
            return (ParseAcl)properties["ACL"];
        }
        set
        {
            properties["ACL"] = value;
        }
    }

    public System.DateTime CreatedAt { get; private set; }

    public System.DateTime UpdatedAt { get; private set; }

    protected Dictionary<string, object> properties = new Dictionary<string, object>();
    public object this[string key]
    {
        get
        {
            return properties[key];
        }
        set
        {
            if (properties.ContainsKey(key))
            {
                properties[key] = value;
            }
            else
            {
                properties.Add(key, value);
            }

        }
    }

    public virtual void DeserializeJson(string json)
    {
        Dictionary<string, object> receivedData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);

        foreach(KeyValuePair<string,object> pair in receivedData)
        {
            if (pair.Key.Equals("objectId"))
            {
                ObjectId = (string)pair.Value;
            }
            else
            if (pair.Key.Equals("createdAt"))
            {
                CreatedAt = (System.DateTime)pair.Value;
            }
            else
            if (pair.Key.Equals("updatedAt"))
            {
                UpdatedAt = (System.DateTime)pair.Value;
            }
            else
            if (pair.Key.Equals("ACL"))
            {
                Acl = JsonConvert.DeserializeObject<ParseAcl>(pair.Value.ToString());
            }
            else
            {
                properties.Add(pair.Key, pair.Value);
            }
        }
    }

    public IEnumerator SaveAsync()
    {
        ParseClient parseClient = ParseClient.Instance;

        Dictionary<string, object> saveData = new Dictionary<string, object>();
        foreach (KeyValuePair<string, object> pair in properties)
        {
            saveData.Add(pair.Key, pair.Value);
        }

        string finalJson = JsonConvert.SerializeObject(saveData);
        Debug.Log(finalJson);


        UnityWebRequest saveRequest = new UnityWebRequest(parseClient.ClassesUrl + this.ClassName, "POST");
        saveRequest.uploadHandler = (UploadHandler)new UploadHandlerRaw(System.Text.Encoding.UTF8.GetBytes(finalJson));
        saveRequest.downloadHandler = new DownloadHandlerBuffer();
        saveRequest.SetRequestHeader("X-Parse-Application-Id", parseClient.AppId);
        saveRequest.SetRequestHeader("X-Parse-Session-Token", ParseUser.CurrentSessionToken);
        saveRequest.SetRequestHeader("Content-Type", parseClient.ContentType);

        yield return saveRequest.SendWebRequest();

        if (saveRequest.isNetworkError)
        {
            Debug.LogWarning(saveRequest.error);
        }
        else
        {
            Debug.Log(saveRequest.downloadHandler.text);
        }
    }
}
