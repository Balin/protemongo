﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Networking;

using Newtonsoft.Json;

[System.Serializable]
public class ParseClient : Singleton<ParseClient> {

    [SerializeField]
    private ParseServerConfig config;
    public bool IsInitialized { get; private set; } = false;
    public string Url { get; private set; }
    public string AppId { get; private set; }

    private const string revocableSession = "1";
    public string RevocableSession => revocableSession;

    private const string contentType = "application/json";
    public string ContentType => contentType;
    public string SignupUrl { get; private set; } = "/users";
    public string LoginUrl { get; private set; } = "/login";
    public string LogoutUrl { get; private set; } = "/logout";
    public string SessionsUrl { get; private set; } = "/sessions";
    public string RolesUrl { get; private set; } = "/roles";
    public string ClassesUrl { get; private set; } = "/classes/";

    // Use this for initialization
    void Awake () {
        GameObject.DontDestroyOnLoad(gameObject);
        
        if(config != null) { Initialize();   }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Initialize()
    {
        Url = config.ServerAddress;
        AppId = config.AppId;

        SignupUrl = Url + "/users";
        LoginUrl = Url + "/login";
        LogoutUrl = Url + "/logout";

        RolesUrl = Url + "/roles";

        ClassesUrl = Url + "/classes/";

        IsInitialized = true;
    }

    
}
