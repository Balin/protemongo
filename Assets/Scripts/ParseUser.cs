﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Networking;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class ParseUser : ParseObject {
    public static ParseUser CurrentUser { get; private set; }
    public bool IsLoggedIn { get; private set; } = false;
    public string SessionToken { get; private set; }
    public static string CurrentSessionToken { get; private set; }

    public delegate void LoginListener(string result);
    public static event LoginListener LoginEventResult;

    public override void DeserializeJson(string json)
    {
        base.DeserializeJson(json);

        Dictionary<string, object> receivedData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);

        foreach (KeyValuePair<string, object> pair in receivedData)
        {
            if (pair.Key.Equals("sessionToken"))
            {
                SessionToken = (string)pair.Value;
                properties.Remove("sessionToken");
            }
        }
    }

    public IEnumerator SignUp(string username, string password)
    {
        ParseClient parseClient = ParseClient.Instance;

        if (!parseClient.IsInitialized)
        {
            Debug.LogError("Parse Client isn't initialized!");
            yield return null;
        }

        if (IsLoggedIn)
        {
            Debug.LogError("Trying to log in an already logged in user, logout first");
            yield return null;
        }

        Debug.Log("signing up");

        Dictionary<string, string> signupData = new Dictionary<string, string>();
        signupData.Add("username", username);
        signupData.Add("password", password);
        string rawBody = JsonConvert.SerializeObject(signupData);
        Debug.Log(rawBody);

        UnityWebRequest signupRequest = new UnityWebRequest(parseClient.SignupUrl, "POST");
        signupRequest.uploadHandler = (UploadHandler)new UploadHandlerRaw(System.Text.Encoding.UTF8.GetBytes(rawBody));
        signupRequest.SetRequestHeader("X-Parse-Application-Id", parseClient.AppId);
        signupRequest.SetRequestHeader("X-Parse-Revocable-Session", parseClient.RevocableSession);
        signupRequest.SetRequestHeader("Content-Type", parseClient.ContentType);

        signupRequest.downloadHandler = new DownloadHandlerBuffer();

        string rawRequest = System.Text.Encoding.Default.GetString(signupRequest.uploadHandler.data);
        Debug.Log(rawRequest);

        yield return signupRequest.SendWebRequest();

        if (signupRequest.isNetworkError)
        {
            Debug.Log(signupRequest.error);
        }
        else
        {
            Debug.Log(signupRequest.downloadHandler.text);
            string receivedJson = System.Text.Encoding.UTF8.GetString(signupRequest.downloadHandler.data);
            if (signupRequest.responseCode == 201)
            {
                DeserializeJson(receivedJson);

                IsLoggedIn = true;
                CurrentSessionToken = SessionToken;
                CurrentUser = this;

                Debug.Log("objectId: " + ObjectId);
                Debug.Log("Created At: " + CreatedAt);
                Debug.Log("Updated At: " + UpdatedAt);

                foreach (KeyValuePair<string, object> pair in properties)
                {
                    Debug.Log(pair.Key + ": " + pair.Value);
                }

                Debug.Log("signup done");

                LoginEventResult("Signed up successfully");
            }
            else
            {
                Dictionary<string, object> receivedData = JsonConvert.DeserializeObject<Dictionary<string, object>>(receivedJson);

                Debug.LogError((string)receivedData["error"]);

                LoginEventResult((string)receivedData["error"]);
            }
        }
    }

    public IEnumerator Login(string username, string password)
    {
        ParseClient parseClient = ParseClient.Instance;

        if (!parseClient.IsInitialized)
        {
            Debug.LogError("Parse Client isn't initialized!");
            yield return null;
        }

        if (IsLoggedIn)
        {
            Debug.LogError("Trying to log in an already logged in user, logout first");
            yield return null;
        }

        Debug.Log("Logging in");

        WWWForm loginForm = new WWWForm();
        loginForm.AddField("username", username);
        loginForm.AddField("password", password);
        string httpFormString = System.Text.Encoding.Default.GetString(loginForm.data);

        UnityWebRequest loginRequest = new UnityWebRequest(parseClient.LoginUrl+"?"+httpFormString, "GET");
        loginRequest.SetRequestHeader("X-Parse-Application-Id", parseClient.AppId);
        loginRequest.SetRequestHeader("X-Parse-Revocable-Session", parseClient.RevocableSession);

        loginRequest.downloadHandler = new DownloadHandlerBuffer();

        yield return loginRequest.SendWebRequest();

        if (loginRequest.isNetworkError)
        {
            Debug.Log(loginRequest.error);
        }
        else
        {
            Debug.Log(loginRequest.downloadHandler.text);
            string receivedJson = System.Text.Encoding.UTF8.GetString(loginRequest.downloadHandler.data);
            if (loginRequest.responseCode == 200)
            {
                DeserializeJson(receivedJson);

                IsLoggedIn = true;
                CurrentSessionToken = SessionToken;
                CurrentUser = this;

                Debug.Log("objectId: " + ObjectId);
                Debug.Log("Created At: " + CreatedAt);
                Debug.Log("Updated At: " + UpdatedAt);

                foreach (KeyValuePair<string, AclPermission> pair in Acl.acl)
                {
                    Debug.Log(pair.Key + ": Read - " + pair.Value.read + " | Write - " + pair.Value.write);
                }

                foreach (KeyValuePair<string, object> pair in properties)
                {
                    Debug.Log(pair.Key + ": " + pair.Value);
                }

                Debug.Log("login done");

                LoginEventResult("Logged in successfully");
            }
            else
            {
                Dictionary<string, object> receivedData = JsonConvert.DeserializeObject<Dictionary<string, object>>(receivedJson);

                Debug.LogError((string)receivedData["error"]);

                LoginEventResult((string)receivedData["error"]);
            }
        }
    }

}
