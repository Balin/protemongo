﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

using UnityEngine;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

[System.Serializable]
public class ParseAcl : ISerializable {

    public Dictionary<string, AclPermission> acl = new Dictionary<string, AclPermission>();

    public ParseAcl()
    {

    }

    public ParseAcl(SerializationInfo info, StreamingContext context)
    {
        foreach(SerializationEntry entry in info)
        {
            JObject aclEntry = (JObject)entry.Value;
            bool readAllowed = false;
            bool writeAllowed = false;

            if (aclEntry.ContainsKey("read"))
            {
                readAllowed = (bool)aclEntry["read"];
            }

            if (aclEntry.ContainsKey("write"))
            {
                writeAllowed = (bool)aclEntry["write"];
            }

            AclPermission aclPermission = new AclPermission(readAllowed, writeAllowed);

            acl.Add(entry.Name, aclPermission);
        }
    }

    public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        foreach(KeyValuePair<string,AclPermission> pair in acl)
        {
            info.AddValue(pair.Key, pair.Value);
        }
    }

    public void DeserializeAclJson(string receivedJson)
    {

    }

    #region aclPublic
    private const string publicNameKey = "*";

    public bool GetPublicReadAccess()
    {
        if (acl.ContainsKey(publicNameKey))
        {
            return acl[publicNameKey].read;
        }
        return false;
    }

    public void SetPublicReadAccess(bool allowed)
    {
        if (acl.ContainsKey(publicNameKey))
        {
            AclPermission aclPermission = acl[publicNameKey];
            aclPermission.read = allowed;
            acl[publicNameKey] = aclPermission;
        }
        else
        {
            acl.Add(publicNameKey, new AclPermission(allowed, false));
        }
    }

    public bool GetPublicWritedAccess()
    {
        if (acl.ContainsKey(publicNameKey))
        {
            return acl[publicNameKey].write;
        }
        return false;
    }

    public void SetPublicWriteAccess(bool allowed)
    {
        if (acl.ContainsKey(publicNameKey))
        {
            AclPermission aclPermission = acl[publicNameKey];
            aclPermission.write = allowed;
            acl[publicNameKey] = aclPermission;
        }
        else
        {
            acl.Add(publicNameKey, new AclPermission(false, allowed));
        }
    }

    public void SetPublicReadWriteAccess(bool readAllowed, bool writeAllowed)
    {
        if (acl.ContainsKey(publicNameKey))
        {
            AclPermission aclPermission = acl[publicNameKey];
            aclPermission.read = readAllowed;
            aclPermission.write = writeAllowed;
            acl[publicNameKey] = aclPermission;
        }
        else
        {
            acl.Add(publicNameKey, new AclPermission(readAllowed, writeAllowed));
        }
    }
    #endregion

    #region aclByUserName
    public bool GetReadAccess(string userId)
    {
        if (acl.ContainsKey(userId))
        {
            return acl[userId].read;
        }
        return false;
    }

    public void SetReadAccess(string userId, bool allowed)
    {
        if (acl.ContainsKey(userId))
        {
            AclPermission aclPermission = acl[userId];
            aclPermission.read = allowed;
            acl[userId] = aclPermission;
        }
        else
        {
            acl.Add(userId, new AclPermission(allowed, false));
        }
    }

    public bool GetWritedAccess(string userId)
    {
        if (acl.ContainsKey(userId))
        {
            return acl[userId].write;
        }
        return false;
    }

    public void SetWriteAccess(string userId, bool allowed)
    {
        if (acl.ContainsKey(userId))
        {
            AclPermission aclPermission = acl[userId];
            aclPermission.write = allowed;
            acl[userId] = aclPermission;
        }
        else
        {
            acl.Add(userId, new AclPermission(false, allowed));
        }
    }

    public void SetReadWriteAccess(string userId, bool readAllowed, bool writeAllowed)
    {
        if (acl.ContainsKey(userId))
        {
            AclPermission aclPermission = acl[userId];
            aclPermission.read = readAllowed;
            aclPermission.write = writeAllowed;
            acl[userId] = aclPermission;
        }
        else
        {
            acl.Add(userId, new AclPermission(readAllowed, writeAllowed));
        }
    }
    #endregion

    #region aclByUserObject
    public bool GetReadAccess(ParseUser user)
    {
        if (acl.ContainsKey(user.ObjectId))
        {
            return acl[user.ObjectId].read;
        }
        return false;
    }

    public void SetReadAccess(ParseUser user, bool allowed)
    {
        AclPermission aclPermission = acl[user.ObjectId];
        aclPermission.read = allowed;
        acl[user.ObjectId] = aclPermission;
    }

    public bool GetWriteAccess(ParseUser user)
    {
        if (acl.ContainsKey(user.ObjectId))
        {
            return acl[user.ObjectId].write;
        }
        return false;
    }

    public void SetWriteAccess(ParseUser user, bool allowed)
    {
        AclPermission aclPermission = acl[user.ObjectId];
        aclPermission.write = allowed;
        acl[user.ObjectId] = aclPermission;
    }

    public void SetReadWriteAccess(ParseUser user, bool readAllowed, bool writeAllowed)
    {
        if (acl.ContainsKey(user.ObjectId))
        {
            AclPermission aclPermission = acl[user.ObjectId];
            aclPermission.read = readAllowed;
            aclPermission.write = writeAllowed;
            acl[user.ObjectId] = aclPermission;
        }
        else
        {
            acl.Add(user.ObjectId, new AclPermission(readAllowed, writeAllowed));
        }
    }
    #endregion

    #region aclByRoleName
    public bool GetRoleReadAccess(string roleName)
    {
        if (acl.ContainsKey(roleName))
        {
            return acl[roleName].read;
        }
        return false;
    }

    public void SetRoleReadAccess(string roleName, bool allowed)
    {
        if (acl.ContainsKey(roleName))
        {
            AclPermission aclPermission = acl[roleName];
            aclPermission.read = allowed;
            acl[roleName] = aclPermission;
        }
        else
        {
            acl.Add(roleName, new AclPermission(allowed, false));
        }
    }

    public bool GetRoleWriteAccess(string roleName)
    {
        if (acl.ContainsKey(roleName))
        {
            return acl[roleName].write;
        }
        return false;
    }

    public void SetRoleWriteAccess(string roleName, bool allowed)
    {
        if (acl.ContainsKey(roleName))
        {
            AclPermission aclPermission = acl[roleName];
            aclPermission.write = allowed;
            acl[roleName] = aclPermission;
        }
        else
        {
            acl.Add(roleName, new AclPermission(allowed, false));
        }
    }

    public void SetRoleReadWriteAccess(string roleName, bool readAllowed, bool writeAllowed)
    {
        if (acl.ContainsKey(roleName))
        {
            AclPermission aclPermission = acl[roleName];
            aclPermission.read = readAllowed;
            aclPermission.write = writeAllowed;
            acl[roleName] = aclPermission;
        }
        else
        {
            acl.Add(roleName, new AclPermission(readAllowed, writeAllowed));
        }
    }
    #endregion

    #region aclByRoleObject
    public bool GetRoleReadAccess(ParseRole role)
    {
        if (acl.ContainsKey(role.Name))
        {
            return acl[role.Name].read;
        }
        return false;
    }

    public void SetRoleReadAccess(ParseRole role, bool allowed)
    {
        AclPermission aclPermission = acl[role.Name];
        aclPermission.read = allowed;
        acl[role.Name] = aclPermission;
    }

    public bool GetRoleWriteAccess(ParseRole role)
    {
        if (acl.ContainsKey(role.Name))
        {
            return acl[role.Name].write;
        }
        return false;
    }

    public void SetRoleWriteAccess(ParseRole role, bool allowed)
    {
        AclPermission aclPermission = acl[role.Name];
        aclPermission.write = allowed;
        acl[role.Name] = aclPermission;
    }

    public void SetRoleReadWriteAccess(ParseRole role, bool readAllowed, bool writeAllowed)
    {
        if (acl.ContainsKey(role.Name))
        {
            AclPermission aclPermission = acl[role.Name];
            aclPermission.read = readAllowed;
            aclPermission.write = writeAllowed;
            acl[role.Name] = aclPermission;
        }
        else
        {
            acl.Add(role.Name, new AclPermission(readAllowed, writeAllowed));
        }
    }
    #endregion
}
