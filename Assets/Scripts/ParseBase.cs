﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

using UnityEngine;

public abstract class ParseObjectBase {
    
}

[System.Serializable]
public struct AclPermission : ISerializable
{
    public AclPermission(bool readAllowed, bool writeAllowed)
    {
        this.read = readAllowed;
        this.write = writeAllowed;
    }

    public bool read;
    public bool write;

    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        info.AddValue("read", read);
        info.AddValue("write", write);
    }
}
