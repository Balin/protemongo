﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParseServerConfig : ScriptableObject {

    [SerializeField]
    private string serverAddress;
    public string ServerAddress
    {
        get
        {
            return serverAddress;
        }
    }

    [SerializeField]
    private string appId;
    public string AppId
    {
        get
        {
            return appId;
        }
    }

}
