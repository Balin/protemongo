﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ParseServerConfigEditor {

	[MenuItem("Assets/Create/Parse Server Config")]
    public static void CreateParseServerConfig()
    {
        ParseServerConfig parseServerConfig = ScriptableObject.CreateInstance<ParseServerConfig>();

        AssetDatabase.CreateAsset(parseServerConfig, "Assets/New Parse Server Config.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = parseServerConfig;
    }
}
