﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParseRole : ParseObject {

    
	public string Name
    {
        get
        {
            return (string)properties["Name"];
        }
        set
        {
            properties["Name"] = value;
        }
    }
}
