﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour {

    public float speed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 currentRotation = transform.localEulerAngles;
        currentRotation.y += speed * Time.deltaTime;
        transform.localEulerAngles = currentRotation;
	}
}
